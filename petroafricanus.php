<?php
/*
Plugin Name: petroafricanus
Plugin URI: http://www.kadimi.com/en/quote
Description: Miscellaneous enhancements
Version: 1.0.0
Author: Nabil Kadimi
Author URI: http://kadimi.com
License: GPL2
*/

// Include Skelet (written by Nabil Kadimi)
include dirname( __FILE__ ) . '/skelet/skelet.php';

// Include options definitions
skelet_dir( dirname( __FILE__ ) . '/data' );

/**
 * Add CORS headers
 */
add_action( 'init', 'petroafricanus_cors' );
function petroafricanus_cors(){

	$o = K::get_var( 'HTTP_ORIGIN', $_SERVER, '' );
	
	$cors_domains = paf( 'petroafricanus_cors' );
	$cors_domains = explode( ',', $cors_domains );
	foreach ( $cors_domains as $k => $v ) {
		$cors_domains[ $k ] = trim( $v );
	}

	if ( in_array( '*', $cors_domains ) || in_array( $o, $cors_domains ) ) {
		header( 'Access-Control-Allow-Origin: *' );
	}
}
