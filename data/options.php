<?php

// Register options page
paf_options( array( 'petroafricanus_cors' => array (
    'page' => 'petroafricanus',
    'title' => __( 'Domain for which to accept CORS' ),
    'description' => '<p class="description">'. 'Comma separated list of domains (<code>*</code> means all domains)' . '</p>',
) ) );
