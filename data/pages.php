<?php

// Register options page
paf_pages( array( 'petroafricanus' => array(
    'parent'      => 'options-general.php',
    'title'       => __( 'PetroAfricanus Options' ),
    'menu_title'  => __( 'PetroAfricanus Options' ),     
) ) );